-- Autocmds are automatically loaded on the VeryLazy event
-- Default autocmds that are always set: https://github.com/LazyVim/LazyVim/blob/main/lua/lazyvim/config/autocmds.lua
-- Add any additional autocmds here

local jenkinsfile_group = vim.api.nvim_create_augroup("JenkinsfileGroup", { clear = true })
vim.api.nvim_create_autocmd({ "BufRead", "BufNewFile" }, {
  command = "set filetype=groovy",
  group = jenkinsfile_group,
  pattern = { "*.jenkinsfile", "Jenkinsfile" },
})
