return {
  {
    "LazyVim/LazyVim",
    opts = {
      colorscheme = "catppuccin",
    },
  },

  {
    "nvim-treesitter/nvim-treesitter",
    opts = function(_, opts)
      -- add tsx and treesitter
      vim.list_extend(opts.ensure_installed, {
        "groovy",
        "clojure",
      })
    end,
  },
  {
    "neovim/nvim-lspconfig",
    ---@class PluginLspOpts
    opts = {
      ---@type lspconfig.options
      servers = {
        groovyls = {},
        clojure_lsp = {},
      },
    },
  },

  {
    "folke/zen-mode.nvim",
    opts = {
      window = {
        width = 150,
      },
    },
    keys = {
      { "<leader>uz", "<cmd>ZenMode<cr>", desc = "ZenMode" },
    },
  },
}
